<section id="showcase">
    <div class="container">
        <h1><?php print($c['title']);?></h1>
    </div>
</section>

<section id="main">
    <div class="container">
        <h3><i class="mainicon fas fa-book fa-2x"></i><?php print($c['bio']);?></h3>
        <p>
            <img class="bandphoto" alt="Bandfoto" src="img/pentagon.jpg"/>
            <?php print($c['p1']);?>
        </p>
        <p>
            <?php print($c['p2']);?>
        </p>
        <p>            
            <?php print($c['p3']);?>
        </p>
        <p>            
            <?php print($c['p4']);?>
        </p>
        <p>
            <?php print($c['p5']);?>
        </p>
    </div>
</section>
