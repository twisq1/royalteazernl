<section id="main">
    <div class="container">
        <h1><?php print $c['h1-1'];?></h1>
        <p><?php print $c['p1-1'];?></p>
    </div>
</section>

<section id="boxes">
    <div class="container">
        <div class="box">
            <i class="boxicon fas fa-music fa-5x"></i>
            <h3><?php print $c['h-own'];?></h3>
            <p><?php print $c['p-own'];?></p>
        </div>
        <div class="box">
            <i class="boxicon fas fa-suitcase fa-5x"></i>
            <h3><?php print $c['h-covers'];?></h3>
            <p><?php print $c['p-covers'];?></p>
        </div>
        <div class="box">
            <i class="boxicon fas fa-smile fa-5x"></i>
            <h3><?php print $c['h-style'];?></h3>
            <p><?php print $c['p-style'];?></p>
        </div>
    </div>
</section>
