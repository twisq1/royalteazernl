<!DOCTYPE html>
<?php
    function merge($a, $b) {
        print (str_replace("%%", $b, $a));
    }

    $l = preg_replace("/[^a-z]/", '', $_GET["lang"]);
    $p = preg_replace("/[^a-z]/", '', $_GET["page"]);
    if ($p == 'index') $p='albumpresentatie';

    include "passwords.php";
    $conn = new mysqli('127.0.0.1', $dbuser, $dbpass, $dbbase);

    $dberr = true;
    $c = array();
    if (!$conn->connect_error) {

        $dberr = false;
        $sql = "SELECT `tag`, `content` FROM `content` WHERE `lang` = '$l' AND (`page` = 'all' OR `page` = '$p')";
        $result = $conn->query($sql);
        while ($row = $result->fetch_assoc()) {
            $c[$row['tag']] = $row['content'];
        }
        $result->free();
    }

    if ($dberr) {
        Header('Location: dberror.php');
    }

    $me = $_SERVER["REQUEST_URI"];
    $nl = preg_replace("/(.*)\/[a-z]{2}\/([a-z]+\.html)/", '$1/nl/$2', $me);
    $en = preg_replace("/(.*)\/[a-z]{2}\/([a-z]+\.html)/", '$1/en/$2', $me);

?>

<html lang="<?php print $l;?>">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width"/>
        <meta name="description" content="<?php print $c['description'];?>"/>
        <meta name="author" content="TWISQ"/>
        <title>Royal Teazer</title>
        <link rel="stylesheet" href="./css/style2.css"/>
        <link rel="stylesheet" href="./css/all.css"/>
        <link rel="icon" href="./img/cropped-t-shirt-logo-32x32.png" sizes="32x32" />
        <link rel="icon" href="./img/cropped-t-shirt-logo-192x192.png" sizes="192x192" />
    </head>
    <body>
        <div class="content">
            <header>
                <div class="container">
                    <div id="branding">
                        <a href="index.html"><img height="72" alt="Royal Teazer logo" src="./img/rtlogo.png"/></a>
                    </div>
                    <nav>
                        <ul>
                        <li class="flag"><a href="<?php print($nl);?>"><img class="flag" src="./img/dutch.png"/></a></li>
                        <li class="flag"><a href="<?php print($en);?>"><img class="flag" src="./img/english.png"/></a></li>
                        </ul>
                    </nav>
                    <nav>
                        <ul>
                            <?php
                                if ($p == 'home') {
                                    print('<li class="current"><a href="index.html">' . $c['home'] . '</a></li>');
                                } else {
                                    print('<li><a href="index.html">' . $c['home'] . '</a></li>');
                                }
                                if ($p == 'band') {
                                    print('<li class="current"><a href="band.html">' . $c['band'] . '</a></li>');
                                } else {
                                    print('<li><a href="band.html">' . $c['band'] . '</a></li>');
                                }
                                if ($p == 'tour') {
                                    print('<li class="current"><a href="tour.html">' . $c['tour'] . '</a></li>');
                                } else {
                                    print('<li><a href="tour.html">' . $c['tour'] . '</a></li>');
                                }
                                if ($p == 'pics') {
                                    print('<li class="current"><a href="pics.html">' . $c['pics'] . '</a></li>');
                                } else {
                                    print('<li><a href="pics.html">' . $c['pics'] . '</a></li>');
                                }
                                if ($p == 'music') {
                                    print('<li class="current"><a href="music.html">' . $c['music'] . '</a></li>');
                                } else {
                                    print('<li><a href="music.html">' . $c['music'] . '</a></li>');
                                }
                                if ($p == 'contact') {
                                    print('<li class="current"><a href="contact.html">' . $c['contact'] . '</a></li>');
                                } else {
                                    print('<li><a href="contact.html">' . $c['contact'] . '</a></li>');
                                }
                            ?>
                        </ul>
                    </nav>
                </div>
            </header>

            <?php
                include "$p.php";
                $conn->close();
            ?>

        </div>
    
        <footer class="footer">
            <p><?php print $c['footer'];?> <a href="https://www.twisq.nl">twisQ</a></p>
        </footer>

        <script>
            let slideIndex = 1;
            
            showSlides(slideIndex);
            console.log("init ", slideIndex);

            // Next/previous controls
            function plusSlides(n) {
            showSlides(slideIndex += n);
            }

            // Thumbnail image controls
            function currentSlide(n) {
            showSlides(slideIndex = n);
            }

            function showSlides(n) {
                let i;
                let slides = document.getElementsByClassName("mySlides");
                let dots = document.getElementsByClassName("demo");
                let captionText = document.getElementById("caption");
                if (n > slides.length) {slideIndex = 1}
                if (n < 1) {slideIndex = slides.length}
                for (i = 0; i < slides.length; i++) {
                    slides[i].style.display = "none";
                }
                for (i = 0; i < dots.length; i++) {
                    dots[i].className = dots[i].className.replace(" active", "");
                }
                slides[slideIndex-1].style.display = "block";
                dots[slideIndex-1].className += " active";
                captionText.innerHTML = dots[slideIndex-1].alt;
            }
        </script>

    </body>
</html>