<section id="showcase">
    <div class="container">
        <h1><?php print($c['title']);?></h1>
    </div>
</section>

<section id="main">
    <div class="container">
        <div class="leftframe">
            <p>
                <?php print($c['p1']);?>
            </p>
            <p>
                <?php print($c['p2']);?>
            </p>
            <p>            
                <?php print($c['p3']);?>
            </p>
            <p>            
                <?php print($c['p4']);?>
                <ul class="fa-ul">
                <li class="bulletlist"><span class="fa-li"><i class="fab fa-spotify"></i></span><a target="_blank" href="https://play.spotify.com/album/5CVzY4Y6CXD5q74fCbBG9i">Spotify</a></li>
                <li class="bulletlist"><span class="fa-li"><i class="fab fa-youtube"></i></span><a target="_blank" href="https://music.youtube.com/watch?v=hfwNzFlAbY4">YouTube music</a></li>
                <li class="bulletlist"><span class="fa-li"><i class="fab fa-itunes"></i></span><a target="_blank" href="https://music.apple.com/us/album/belo-horizonte-single/1510846376?uo=4">iTunes</a></li>
                <li class="bulletlist"><span class="fa-li"><i class="fas fa-music"></i></span><a target="_blank" href="https://www.deezer.com/album/145313292?utm_source=deezer&utm_content=album-145313292&utm_term=0_1588216495&utm_medium=web">Deezer</a></li>
                <li class="bulletlist"><span class="fa-li"><i class="fab fa-amazon"></i></span><a target="_blank" href="https://www.amazon.com/dp/B087WPTGH3">Amazon</a></li>
                </ul>
                <br/><br/>
            </p>
            <p>
                <?php print($c['lyrics']);?>
                <pre class="lyrics">
Het blijft maar regenen
Het houdt niet op
De lucht is donkergrijs
Zwart, in mijn kop
De wolken jagen door de lucht
Bomen buigen door
Mee met de wind
Blad'ren volgen het spoor
Vliegen gezwind
De wind jaagt alles op de vlucht

Doe je ogen open want
Hier is de zon
Droog je tranen nu maar want
Hier is de zon

Alles is anders nu
Want de zon schijnt
Maar wat gebeurt er als
De zon weer verdwijnt
Het licht is mooier dan het ooit was
Is alles beter nu?
Is alles fijn?
Of lijkt dat maar zo
En is het schijn
Het licht lijkt mooier dan het ooit was

Doe je ogen open want
Hier is de zon
Droog je tranen nu maar want
Hier is de zon

De storm die waait vanzelf weer over
De wolken hebben alle tijd
Geduldig wachtend op de zomer
Zodat ook ons gemoed gedijt
We groeien en we worden sterker
We wapenen ons meer en meer
Het donker kan ons niet meer raken
We zijn bestand tegen elk weer
                </pre>
            </p>
        </div>

        <div class="rightframe">
            <a target="_blank" href="https://play.spotify.com/album/5CVzY4Y6CXD5q74fCbBG9i">
                <img class="coverart" alt="Belo Horizonte album cover" src="img/belohorizonte348.jpg"/>
            </a>
            <p>
                <?php print($c['credits']);?>
            </p>
        </div>
    </div>

</section>
