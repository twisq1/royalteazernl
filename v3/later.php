<section id="showcase">
    <div class="container">
        <h1><?php print($c['title']);?></h1>
    </div>
</section>

<section id="main">
    <div class="container">
        <div class="leftframe">
            <p>
                <?php print($c['p1']);?>
            </p>
            <p>
                <?php print($c['p2']);?>
            </p>
            <p>            
                <?php print($c['p3']);?>
            </p>
            <p>            
                <?php print($c['p4']);?>
                <ul class="fa-ul">
                <li class="bulletlist"><span class="fa-li"><i class="fab fa-spotify"></i></span><a target="_blank" href="https://open.spotify.com/track/4yhDdjzPvDo5EdMMvhDMW5">Spotify</a></li>
                <li class="bulletlist"><span class="fa-li"><i class="fab fa-youtube"></i></span><a target="_blank" href="https://music.youtube.com/watch?v=rOnTlDrdSF0">YouTube</a></li>
                </ul>
                <br/><br/>
            </p>
            <p>
                <?php print($c['lyrics']);?>
                <pre class="lyrics">
Ik zie je later
Is een zin de je vaak hoort 
Vaak als eind van een gesprek
En daarna er snel vandoor
Ik zie je later
En met een beetje goede wil
Hoor je ooit nog wel eens iets
Maar meestal blijft het stil

Later, Ik zie je later
Later, Ik zie je later

Ik spreek je later
Klinkt als “U hoort nog van ons”
En krijg je net als op je eerste date
Eigenlijk de bons
Ik spreek je later
Nee je hoeft ons niet te bellen
Wanneer we alles op een rijtje hebben
Gaan we het vertellen

Later, Ik zie je later
Later, Ik zie je later

Nee, je hoeft mij niet te bellen
We houden contact
Mijn afspraak zit te wachten
We houden contact
Ik moet nu echt vertrekken
We houden contact
Het was echt een goed gesprek
We houden contact

Er is geen reden voor een lange pauze
We houden contact
Elkaar verliezen gaat ons niet gebeuren
We houden contact
Het is nu tijd om te vertrekken
We houden contact
We spreken af om iets te drinken
We houden contact
                </pre>
            </p>
        </div>

        <div class="rightframe">
            <a target="_blank" href="">
                <img class="coverart" alt="Later album cover" src="img/later348.png"/>
            </a>
            <p>
                <?php print($c['credits']);?>
            </p>
        </div>
    </div>

</section>
