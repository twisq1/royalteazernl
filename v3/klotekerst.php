<section id="showcase">
    <div class="container">
        <h1><?php print($c['title']);?></h1>
    </div>
</section>

<section id="main">
    <div class="container">
        <div class="leftframe">
            <p>
                <?php print($c['p1']);?>
            </p>
            <p>
                <?php print($c['p2']);?>
            </p>
            <p>            
                <?php print($c['p3']);?>
            </p>
            <p>            
                <?php print($c['p4']);?>
                <ul class="fa-ul">
                <li class="bulletlist"><span class="fa-li"><i class="fab fa-youtube"></i></span><a target="_blank" href="https://youtu.be/Je27DuIqt84">YouTube</a></li>
                <li class="bulletlist"><span class="fa-li"><i class="fab fa-spotify"></i></span><a target="_blank" href="https://open.spotify.com/track/1s8sm1bFzjCLPLeqiKBlQ8">Spotify</a></li>
                </ul>
                <br/><br/>
            </p>
            <p>
                <?php print($c['lyrics']);?>
                <pre class="lyrics">
Wat naar voor je meid
Wat een klotetijd
Dit was niet voorbereid
En ik hoop niet dat je lijdt

Wat verschrikkeluk
Dit is echt mega ruk
Je hebt echt geen geluk
Zo te zien zit je stuk

Wat een klotekerst, een klote klote klotekerst
Wat een klotekerst, een klote klote klotekerst
Het is voor iedereen een strop
Ook ellende houdt eens op

Wat een mooie boom
Het is buitengewoon
Mooi versierd, design misschien?
Maar niet veel zullen hem zien

Aan het kerstdiner
Doen niet veel mensen mee
Met een klein clubje aan de dis
Niet het ergste wat er is

Wat een klotekerst, een klote klote klotekerst
Wat een klotekerst, een klote klote klotekerst
Het is voor iedereen een strop
Ook ellende houdt eens op
Eet verdomme die kerststol op!
                </pre>
            </p>
        </div>

        <div class="rightframe">
            <a target="_blank" href="https://open.spotify.com/album/1fYZoYK4TSHr2CyKpC57al">
                <img class="coverart" alt="Klotekerst album cover" src="img/klotekerst348.png"/>
            </a>
            <p>
                <?php print($c['credits']);?>
            </p>
        </div>
    </div>

</section>
