<?php

    $up = array();
    $down = array();
    $sql = "SELECT DATE_FORMAT(`start`,'%a') AS `weekday`,DATE_FORMAT(`start`,'%d-%m-%Y') AS `date`,DATE_FORMAT(`start`,'%H:%i') AS `time`,`venue`,`url`,(`start`>NOW()) as `future` FROM `shows` ORDER BY `start`";
    $result = $conn->query($sql);
    while ($row = $result->fetch_assoc()) {
        if ($row['future']) array_push($up, $row);
        else array_push($down, $row);
    }
    $result->free();

?>

<section id="showcase">
    <div class="container">
        <h1><?php print($c['title']);?></h1>
    </div>
</section>

<section id="main">
    <div class="container">
        <p>
        <!--
        <iframe width="1280" height="720" src="https://www.youtube.com/embed/KrRbuCANY4M" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 
            <br/>
            Helaas is optreden onder de huidige omstandigheden niet mogelijk. Wel hebben we deze video van ons laatste optreden in het Patronaat.<br/>
        -->
            <?php
                print('<table>');
                print('<tbody>');
                print('<tr>');
                print('<td colspan="5">');
                print('<h3><i class="mainicon fas fa-volume-up fa-2x"></i>' . $c['future'] . '</h3>');
                print('</td>');
                print('</tr>');
                if (count($up)) {
                    for($i = 0; $i < count($up) ; $i++) {
                        $row = $up[$i];
                        print('<tr>');
                        print('<td>');
                        print($c[$row['weekday']]);
                        print('</td>');
                        print('<td>');
                        print($row['date']);
                        print('</td>');
                        print('<td>');
                        print($row['time']);
                        print('</td>');
                        print('<td>');
                        print($row['venue']);
                        print('</td>');
                        print('<td>');
                        print('<a target="_blank" href="' . $row['url'] . '"><i class="fas fa-search"></i></a>');
                        print('</td>');
                        print('</tr>');
                    }
                } else {
                    print('<tr>');
                    print('<td colspan="5">');
                    print($c['noshow']);
                    print('</td>');
                    print('<tr>');
                }

                print('<tr>');
                print('<td colspan="5">');
                print('<h3><i class="mainicon fas fa-volume-down fa-2x"></i>' . $c['past'] . '</h3>');
                print('</td>');
                print('<tr>');
                for($i = count($down) - 1; $i >=0 ; $i--) {
                    $row = $down[$i];
                    print('<tr>');
                    print('<td class="past">');
                    print($c[$row['weekday']]);
                    print('</td>');
                    print('<td class="past">');
                    print($row['date']);
                    print('</td>');
                    print('<td class="past">');
                    print($row['time']);
                    print('</td>');
                    print('<td class="past">');
                    print($row['venue']);
                    print('</td>');
                    print('<td class="past">');
                    print('<a target="_blank" href="' . $row['url'] . '"><i class="fas fa-search"></i></a>');
                    print('</td>');
                    print('</tr>');
                }
                print('</tbody>');
                print('</table>');
            ?>
        </p>
    </div>
</section>
