<section id="showcase">
    <div class="container">
        <h1><?php print($c['title']);?></h1>
    </div>
</section>

<section id="main">
    <div class="container">
        <div class="leftframe">
            <p>
                <?php print($c['p1']);?>
            </p>
            <p>
                <?php print($c['p2']);?>
            </p>
            <p>            
                <?php print($c['p3']);?>
            </p>
            <p>            
                <?php print($c['p4']);?>
                <ul class="fa-ul">
                <li class="bulletlist"><span class="fa-li"><i class="fab fa-spotify"></i></span><a target="_blank" href="https://open.spotify.com/track/6vkF6aHOJ9YYN9E9vEiOpg">Spotify</a></li>
                <li class="bulletlist"><span class="fa-li"><i class="fab fa-youtube"></i></span><a target="_blank" href="https://youtu.be/xtvjIUNOcGg">YouTube</a></li>
                </ul>
                <br/><br/>
            </p>
            <p>
                <?php print($c['lyrics']);?>
                <pre class="lyrics">
Zeg me wat de consequenties zijn van dit besluit
Zonder inbreng van een ander kom ik er niet uit
Wat gaat het betekenen op de lange termijn?
Help me het uit te vinden. Ik moet er binnenkort uit zijn

Houd ik wat ik heb, of ruil ik het in?
Ga ik achterlaten wat ik al jaren bemin?
Zet ik er een punt achter, voor een ander doel?
Er is een flinke strijd gaande met mijn gevoel

Kiezen is niet makkelijk, dat is nou juist de clou
Het heeft altijd gevolgen, wat je ook doet
Ook niet kiezen is een keuze, denk daar maar over na
Het feit dat ik kan kiezen komt doordat ik besta

Niet of wel, wel of niet, het gaat om hoe je de toekomst ziet
Niet of wel, wel of niet, het gaat om hoe je de toekomst ziet

De teerling is geworpen, de hamer die is neer
Het elke nacht weer wakker liggen, hoeft nu niet meer
De beslissing is genomen, ik kan nu niet meer terug
Het hoofdstuk is gesloten, het is achter de rug

Niet of wel, wel of niet, het gaat om hoe je de toekomst ziet
Niet of wel, wel of niet, het gaat om hoe je de toekomst ziet
                </pre>
            </p>
        </div>

        <div class="rightframe">
            <a target="_blank" href="">
                <img class="coverart" alt="Kiezen album cover" src="img/kiezen348.png"/>
            </a>
            <p>
                <?php print($c['credits']);?>
            </p>
        </div>
    </div>

</section>
