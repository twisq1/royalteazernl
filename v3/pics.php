<?php

    $photos = array();
    $sql = "SELECT `bigfile`,`smallfile`,`copyright`,`description` FROM `photo`";
    $result = $conn->query($sql);
    while ($row = $result->fetch_assoc()) {
        array_push($photos, $row);
    }
    $result->free();

?>

<section id="main">
    <!-- Container for the image gallery -->
    <div class="photo-container">
        <?php
            $total=count($photos);
            for($i = 0; $i < count($photos) ; $i++) {
                $row = $photos[$i];
                $big=$row['bigfile'];
                $small=$row['smallfile'];
                $copyright=$row['copyright'];
                $alt=$row['description'];
                $slide=$i+1;

                /* Full-width images with number text */
                print('<div class="mySlides">');
                print("<div class='numbertext'>$slide / $total</div>");
                print("<img src='photo/$big' style='display: block;margin-left: auto;margin-right: auto;width:75%'>");
                print('</div>');
            }
        ?>

        <!-- Next and previous buttons -->
        <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
        <a class="next" onclick="plusSlides(1)">&#10095;</a>

        <!-- Image text -->
        <div class="caption-container">
            <p id="caption"></p>
        </div>

        <!-- Thumbnail images -->
        <div class="row">
            <?php
                for($i = 0; $i < count($photos) ; $i++) {
                    $row = $photos[$i];
                    $big=$row['bigfile'];
                    $small=$row['smallfile'];
                    $copyright=$row['copyright'];
                    $alt=$row['description'] . ' - Foto: ' . $row['copyright'];
                    $slide=$i+1;

                    /* Full-width images with number text */
                    print('<div class="column">');
                    print("<img class='demo cursor' src='photo/$small' style='width:100%' onclick='currentSlide($slide)' alt='$alt'>");
                    print('</div>');
                }
            ?>
        </div>
    </div>

</section>
