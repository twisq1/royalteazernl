<section id="showcase">
    <div class="container">
        <h1><?php print($c['title']);?></h1>
    </div>
</section>

<section id="main">
    <div class="container">
        <div class="leftframe">
            <p>
                <?php print($c['p1']);?>
            </p>
            <p>
                <?php print($c['p2']);?>
            </p>
            <p>            
                <?php print($c['p3']);?>
            </p>
            <p>            
                <?php print($c['p4']);?>
                <ul class="fa-ul">
                <li class="bulletlist"><span class="fa-li"><i class="fab fa-youtube"></i></span><a target="_blank" href="https://youtu.be/gww7eFiWqdM">YouTube</a></li>
                <li class="bulletlist"><span class="fa-li"><i class="fab fa-spotify"></i></span><a target="_blank" href="https://open.spotify.com/album/1fYZoYK4TSHr2CyKpC57al">Spotify</a></li>
                <li class="bulletlist"><span class="fa-li"><i class="fab fa-itunes"></i></span><a target="_blank" href="https://music.apple.com/nl/album/beach-single/1527593138?l=en">iTunes</a></li>
                <li class="bulletlist"><span class="fa-li"><i class="fab fa-amazon"></i></span><a target="_blank" href="https://www.amazon.com/dp/B08FTLP53G">Amazon</a></li>
                </ul>
                <br/><br/>
            </p>
            <p>
                <?php print($c['lyrics']);?>
                <pre class="lyrics">
A happy life under the sea
Together with my family
Singing between the seaweed
Now I don't know what happened to me

My name was written in the sand
My love was drawn onto the land
All was about to change
In this world so beautiful and strange

My mother warned me not to listen to this cry
She knew the consequensces, did not tell a lie
My father learned me to have faith in myself
He knew one day I would need somebody else

A new life in a beautiful land
Living with a good man
But when we walked together on the beach
I could hear my crying family

And now I was torn in two
Did not know what I had to do
But when I stood there on the beach
I wanted back to my family
                </pre>
            </p>
        </div>

        <div class="rightframe">
            <a target="_blank" href="https://open.spotify.com/album/1fYZoYK4TSHr2CyKpC57al">
                <img class="coverart" alt="Beach album cover" src="img/beach348.png"/>
            </a>
            <p>
                <?php print($c['credits']);?>
            </p>
        </div>
    </div>

</section>
