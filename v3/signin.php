<?php

	if (!($stmt = $conn->prepare("INSERT INTO `mailing` (`email`, `ip`, `created`) VALUES (?, ?, NOW())"))) {
		echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
		exit;
	}
	if (!$stmt->bind_param('ss', $email, $ip)) {
		echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
		exit;
	}	

	$ip = $conn->real_escape_string($_SERVER['REMOTE_ADDR']);
	$email = $_POST['mail'];
	
	if (!$stmt->execute()) {
		echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
		exit;
	}
	
	$stmt->close();

?>

<section id="showcase">
    <div class="container">
        <h1><?php print($c['title']);?></h1>
    </div>
</section>

<section id="main">
    <div class="container">
        <p>
            <img class="bandphoto" alt="Bandfoto" src="img/pentagon.jpg"/>
            <?php print($c['p1']);?>
        </p>
    </div>
</section>
