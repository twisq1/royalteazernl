<section id="showcase">
    <div class="container">
        <h1><?php print $c['title'];?></h1>
        <p>
            <?php merge($c['intro'], '<a target="_blank" href="https://open.spotify.com/user/twisq1/playlist/4i4iZlKLkLiXMbjeYKUeoo"><i class="fab fa-spotify"></i> Spotify</a>');?>
        </p>
    </div>
</section>

<section id="main">
    <div class="container">
        <div class="music-list">

            <div class="music-item">
                <div class="music-picture">
                    <a href="later.html"><img src="img/later348.png" alt="Later album cover"/></a>
                </div>
                <div class="music-description">
                    <h3>Kiezen</h3>
                    <p>
                        <a target="_blank" href="https://open.spotify.com/track/4yhDdjzPvDo5EdMMvhDMW5"><i class="fab fa-spotify"></i> <?php merge($c['spotify'], 'Later');?></a><br/>
                        <a target="_blank" href="https://music.youtube.com/watch?v=rOnTlDrdSF0"><i class="fab fa-youtube"></i> <?php merge($c['youtubemusic'], 'Later');?></a><br/>
                        <a href="later.html"><?php print $c['more'];?>...</a><br/>
                    </p>
                </div>
            </div>

            <div class="music-item">
                <div class="music-picture">
                    <a href="kiezen.html"><img src="img/kiezen348.png" alt="Kiezen album cover"/></a>
                </div>
                <div class="music-description">
                    <h3>Kiezen</h3>
                    <p>
                        <a target="_blank" href="https://open.spotify.com/track/6vkF6aHOJ9YYN9E9vEiOpg"><i class="fab fa-spotify"></i> <?php merge($c['spotify'], 'Kiezen');?></a><br/>
                        <a target="_blank" href="https://youtu.be/xtvjIUNOcGg"><i class="fab fa-youtube"></i> <?php merge($c['youtubemusic'], 'Kiezen');?></a><br/>
                        <a href="kiezen.html"><?php print $c['more'];?>...</a><br/>
                    </p>
                </div>
            </div>

            <div class="music-item">
                <div class="music-picture">
                    <a href="klotekerst.html"><img src="img/klotekerst348.png" alt="Klotekerst album cover"/></a>
                </div>
                <div class="music-description">
                    <h3>Klotekerst</h3>
                    <p>
                        <a target="_blank" href="https://youtu.be/Je27DuIqt84"><i class="fab fa-youtube"></i> <?php merge($c['youtube'], 'Klotekerst');?></a><br/>
                        <a target="_blank" href="https://open.spotify.com/track/1s8sm1bFzjCLPLeqiKBlQ8"><i class="fab fa-spotify"></i> <?php merge($c['spotify'], 'Klotekerst');?></a><br/>
                        <a href="klotekerst.html"><?php print $c['more'];?>...</a><br/>
                    </p>
                </div>
            </div>

            <div class="music-item">
                <div class="music-picture">
                    <a href="beach.html"><img src="img/beach348.png" alt="Beach album cover"/></a>
                </div>
                <div class="music-description">
                    <h3>Beach</h3>
                    <p>
                        <a target="_blank" href="https://youtu.be/gww7eFiWqdM"><i class="fab fa-youtube"></i> <?php merge($c['youtube'], 'Beach');?></a><br/>
                        <a target="_blank" href="https://open.spotify.com/album/1fYZoYK4TSHr2CyKpC57al"><i class="fab fa-spotify"></i> <?php merge($c['spotify'], 'Beach');?></a><br/>
                        <a href="beach.html"><?php print $c['more'];?>...</a><br/>
                    </p>
                </div>
            </div>

            <div class="music-item">
                <div class="music-picture">
                    <a href="belohorizonte.html"><img src="img/belohorizonte348.jpg" alt="Belo Horizonte album cover"/></a>
                </div>
                <div class="music-description">
                    <h3>Belo Horizonte</h3>
                    <p>
                        <a target="_blank" href="https://play.spotify.com/album/5CVzY4Y6CXD5q74fCbBG9i"><i class="fab fa-spotify"></i> <?php merge($c['spotify'], 'Belo Horizonte');?></a><br/>
                        <a href="belohorizonte.html"><?php print $c['more'];?>...</a><br/>
                    </p>
                </div>
            </div>

            <div class="music-item">
                <div class="music-picture">
                    <img src="img/contemporaryhero348.jpg" alt="Plaatje van Contemporary Hero"/>
                </div>
                <div class="music-description">
                    <h3>Contemporary Hero</h3>
                    <p>
                        <a href="https://open.spotify.com/album/7IIIPPkIx0RsAAXGIIctsP"><i class="fab fa-spotify"></i> <?php merge($c['spotify'], 'Contemporary Hero');?></a><br/>
                        <a href="https://itunes.apple.com/nl/album/contemporary-hero-single/1441254932?l=en"><i class="fab fa-itunes"></i> <?php merge($c['itunes'], 'Contemporary Hero');?></a><br/>
                        <a href="https://www.youtube.com/watch?v=XVtECO_O7jY"><i class="fab fa-youtube"></i> <?php merge($c['youtubemusic'], 'Contemporary Hero');?></a>
                    </p>
                </div>
            </div>
            <div class="music-item">
                <div class="music-picture">
                    <img src="img/notime348.jpg" alt="Plaatje van No Time"/>
                </div>
                <div class="music-description">
                    <h3>No Time</h3>
                    <p>
                        <a href="https://open.spotify.com/album/5PB8VQD9zbkhozzjXzxvJ5"><i class="fab fa-spotify"></i> <?php merge($c['spotify'], 'No Time');?></a><br/>
                        <a href="https://itunes.apple.com/nl/album/no-time-single/1441330335?l=en"><i class="fab fa-itunes"></i> <?php merge($c['itunes'], 'No Time');?></a><br/>
                        <a href="https://www.youtube.com/watch?v=56pfvNZ2jWQ"><i class="fab fa-youtube"></i> <?php merge($c['youtubemusic'], 'No Time');?></a>
                    </p> 
                </div>
            </div>
            <div class="music-item">
                <div class="music-picture">
                    <img src="img/thatfeeling.png" alt="Plaatje van That Feeling"/>
                </div>
                <div class="music-description">
                    <h3>That Feeling</h3>
                    <p>
                        <a href="https://youtu.be/b9CeV09N4RU"><i class="fab fa-youtube"></i> <?php merge($c['youtube'], 'That Feeling');?></a><br/>
                        <a href="https://open.spotify.com/track/3NXAwXRua81afQsNEhmzR7"><i class="fab fa-spotify"></i> <?php merge($c['spotify'], 'That Feeling');?></a><br/>
                        <a href="https://itunes.apple.com/nl/album/that-feeling/1019342343?i=1019342647&l=en"><i class="fab fa-itunes"></i> <?php merge($c['itunes'], 'That Feeling');?></a>
                    </p>
                </div>
            </div>
            <div class="music-item">
                <div class="music-picture">
                    <img src="img/cynical348.jpg" alt="Plaatje van Cynical"/>
                </div>
                <div class="music-description">
                    <h3>Cynical</h3>
                    <p>
                        <a href="https://open.spotify.com/album/6g9yvTwN9SHNN8L3ZKeeU1"><i class="fab fa-spotify"></i> <?php merge($c['spotify'], 'Cynical');?></a><br/>
                        <a href="https://itunes.apple.com/nl/album/cynical-single/1137280403?l=en"><i class="fab fa-itunes"></i> <?php merge($c['itunes'], 'Cynical');?></a>    
                    </p>
                </div>
            </div>
            <div class="music-item">
                <div class="music-picture">
                    <img src="img/arizona348.jpg" alt="Plaatje van Arizona"/>
                </div>
                <div class="music-description">
                    <h3>Arizona</h3>
                    <p>
                        <a href="https://open.spotify.com/album/4OqRdVwYedoEWK6jWegQft"><i class="fab fa-spotify"></i> <?php merge($c['spotify'], 'Arizona');?></a><br/>
                        <a href="https://itunes.apple.com/nl/album/arizona-single/1136945675?l=en"><i class="fab fa-itunes"></i> <?php merge($c['itunes'], 'Arizona');?></a>
                    </p> 
                </div>
            </div>
            <div class="music-item">
                <div class="music-picture">
                    <img src="img/RoyalTeazerInConcretoCover1440-1024x1024.png" alt="Plaatje bij Happy Now"/>
                </div>
                <div class="music-description">
                    <h3>Happy Now</h3>
                    <p>
                        <a href="https://open.spotify.com/track/6rHZsur0MR3r1tXeVb0VYs"><i class="fab fa-spotify"></i> <?php merge($c['spotify'], 'Happy Now');?></a><br/>
                        <a href="https://itunes.apple.com/nl/album/happy-now/1019342343?i=1019342656&l=en"><i class="fab fa-itunes"></i> <?php merge($c['itunes'], 'Happy Now');?>.</a>
                    </p> 
                </div>
            </div>
            <div class="music-item">
                <div class="music-picture">
                    <img src="img/RoyalTeazerInConcretoCover1440-1024x1024.png" alt="Plaatje bij Brand New Day"/>
                </div>
                <div class="music-description">
                    <h3>Brand New Day</h3>
                    <p>
                        <a href="https://open.spotify.com/track/7FcbIp3q5GX4aN8PS3hDH2"><i class="fab fa-spotify"></i> <?php merge($c['spotify'], 'Brand New Day');?></a><br/>
                        <a href="https://itunes.apple.com/nl/album/brand-new-day/1019342343?i=1019342652&l=en"><i class="fab fa-itunes"></i> <?php merge($c['itunes'], 'Brand New Day');?></a>
                    </p> 
                </div>
            </div>
            <div class="music-item">
                <div class="music-picture">
                    <img src="img/RoyalTeazerInConcretoCover1440-1024x1024.png" alt="Plaatje bij Perfect Crime"/>
                </div>
                <div class="music-description">
                    <h3>Perfect Crime</h3>
                    <p>
                        <a href="https://open.spotify.com/track/1pqiU1OUE3YBM6LO4BMPLK"><i class="fab fa-spotify"></i> <?php merge($c['spotify'], 'Perfect Crime');?></a><br/>
                        <a href="https://itunes.apple.com/nl/album/perfect-crime/1019342343?i=1019342657&l=en"><i class="fab fa-itunes"></i> <?php merge($c['itunes'], 'Perfect Crime');?></a>
                    </p> 
                </div>
            </div>
        </div>
    </div>
</section>