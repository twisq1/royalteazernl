<section id="showcase">
    <div class="container">
        <h1><?php print($c['title']);?></h1>
    </div>
</section>

<section id="main">
    <div class="container">
        <h3><i class="mainicon fas fa-music fa-2x"></i><?php print($c['head']);?></h3>
        <p>
            <table>
                <tbody>
                    <tr>
                        <td>All the small things</td>
                        <td>Blink 182</td>
                        <td><a href="http://open.spotify.com/track/12qZHAeOyTf93YAWvGDTat"><i class="fab fa-spotify"></i></a></td>
                    </tr>
                    <tr>
                        <td>Arizona</td>
                        <td>Royal Teazer</td>
                        <td><a href="https://open.spotify.com/album/4OqRdVwYedoEWK6jWegQft"><i class="fab fa-spotify"></i></a></td>
                    </tr>
                        <tr>
                        <td>Baby when you&#8217;re gone</td>
                        <td>Bryan Adams and Melany C.</td>
                        <td><a href="http://open.spotify.com/track/7p9dd71JR2ucoAuO1Sy0VZ"><i class="fab fa-spotify"></i></a></td>
                    </tr>
                    <tr>
                        <td>Big me</td>
                        <td>Foo Fighters</td>
                        <td><a href="http://open.spotify.com/track/04Jk0uP8SbDHLyAKfRctNQ"><i class="fab fa-spotify"></i></a></td>
                    </tr>
                    <tr>
                        <td>Bohemian like you</td>
                        <td>The Dandy Warhols</td>
                        <td><a href="http://open.spotify.com/track/7jA4agDnt1Oc6Fn2hifX0t"><i class="fab fa-spotify"></i></a></td>
                    </tr>
                    <tr>
                        <td>Breakfast at Tiffany&#8217;s</td>
                        <td>Deep blue something</td>
                        <td><a href="http://open.spotify.com/track/6nRV46vbiHE5tTnS4VmKk7"><i class="fab fa-spotify"></i></a></td>
                    </tr>
                    <tr>
                        <td>Buddy Holly</td>
                        <td>Weezer</td>
                        <td><a href="https://open.spotify.com/track/3mwvKOyMmG77zZRunnxp9E"><i class="fab fa-spotify"></i></a></td>
                    </tr>
                    <tr>
                        <td>Call me</td>
                        <td>Blondie</td>
                        <td><a href="http://open.spotify.com/track/0vLwL4xuJ3s7SeaCdvMqkY"><i class="fab fa-spotify"></i></a></td>
                    </tr>
                    <tr>
                        <td>Cynical</td>
                        <td>Royal Teazer</td>
                        <td><a href="https://open.spotify.com/album/6g9yvTwN9SHNN8L3ZKeeU1"><i class="fab fa-spotify"></i></a></td>
                    </tr>
                    <tr>
                        <td>Dream all day</td>
                        <td>The Posies</td>
                        <td><a href="http://open.spotify.com/track/7l20mFvtB4Ffb1wTuLbc7P"><i class="fab fa-spotify"></i></a></td>
                    </tr>
                    <tr>
                        <td>Eight days a week</td>
                        <td>The Beatles</td>
                        <td><a href="http://open.spotify.com/track/4KnwJk8C1dJ8XVr9V7EZJ7"><i class="fab fa-spotify"></i></a></td>
                    </tr>
                    <tr>
                        <td>Everyday I work on the road</td>
                        <td>Voicst</td>
                        <td><a href="http://open.spotify.com/track/3Ub3RQM9o5ZASDNqjNTjpk"><i class="fab fa-spotify"></i></a></td>
                    </tr>
                    <tr>
                        <td>I kissed a girl</td>
                        <td>Katy Perry</td>
                        <td><a href="https://open.spotify.com/track/14iN3o8ptQ8cFVZTEmyQRV"><i class="fab fa-spotify"></i></a></td>
                    </tr>
                    <tr>
                        <td>I&#8217;ll be there for you</td>
                        <td>The Rembrandts</td>
                        <td><a href="http://open.spotify.com/track/1lfjTOtTRUDkzcmahA4lcs"><i class="fab fa-spotify"></i></a></td>
                    </tr>
                    <tr>
                        <td>In my place</td>
                        <td>Coldplay</td>
                        <td><a href="http://open.spotify.com/track/2nvC4i2aMo4CzRjRflysah"><i class="fab fa-spotify"></i></a></td>
                    </tr>
                    <tr>
                        <td>Je suis parti</td>
                        <td>The Madd</td>
                        <td><a href="http://open.spotify.com/track/09dgYZNjCu2yFFFRK6wnED"><i class="fab fa-spotify"></i></a></td>
                    </tr>
                    <tr>
                        <td>Little arithmetics</td>
                        <td>dEUS</td>
                        <td><a href="http://open.spotify.com/track/2YKZ9avgT145BkCasAfEIQ"><i class="fab fa-spotify"></i></a></td>
                    </tr>
                    <tr>
                        <td>Locked out of heaven</td>
                        <td>Bruno Mars</td>
                        <td><a href="http://open.spotify.com/track/5PsMbxhgWpJMsouEfDTX6r"><i class="fab fa-spotify"></i></a></td>
                    </tr>
                    <tr>
                        <td>Lonely boy</td>
                        <td>The Black Keys</td>
                        <td><a href="http://open.spotify.com/track/3dOAXUx7I1qnzWzxdnsyB8"><i class="fab fa-spotify"></i></a></td>
                    </tr>
                    <tr>
                        <td>Mrs Robinson</td>
                        <td>The Lemonheads</td>
                        <td><a href="http://open.spotify.com/track/3BzEDIWG446coOBAjehV1z"><i class="fab fa-spotify"></i></a></td>
                    </tr>
                    <tr>
                        <td>Pretty Woman</td>
                        <td>Roy Orbison</td>
                        <td><a href="https://open.spotify.com/track/1cSb92qZ95Kp9TqCI0caPx"><i class="fab fa-spotify"></i></a></td>
                    </tr>
                    <tr>
                        <td>Simone</td>
                        <td>The Kik</td>
                        <td><a href="http://open.spotify.com/track/6yddazDVfoFir5aUeqlVU0"><i class="fab fa-spotify"></i></a></td>
                    </tr>
                    <tr>
                        <td>Song 2</td>
                        <td>Blur</td>
                        <td><a href="http://open.spotify.com/track/3GfOAdcoc3X5GPiiXmpBjK"><i class="fab fa-spotify"></i></a></td>
                    </tr>
                    <tr>
                        <td>Suddenly I see</td>
                        <td>KT Tunstall</td>
                        <td><a href="http://open.spotify.com/track/5p9XWUdvbUzmPCukOmwoU3"><i class="fab fa-spotify"></i></a></td>
                    </tr>
                    <tr>
                        <td>Valerie</td>
                        <td>The Zutons</td>
                        <td><a href="http://open.spotify.com/track/2LuJkoxMlLQshNTVa8oCXW"><i class="fab fa-spotify"></i></a></td>
                    </tr>
                    <tr>
                        <td>Video killed the radio star</td>
                        <td>Presidents of the USA</td>
                        <td><a href="http://open.spotify.com/track/7wck5ORA8dJB8To9rc10gi"><i class="fab fa-spotify"></i></a></td>
                    </tr>
                    <tr>
                        <td>Where is my mind</td>
                        <td>Pixies</td>
                        <td><a href="http://open.spotify.com/track/5BP0oaQ1VhuaznT77CBXQp"><i class="fab fa-spotify"></i></a></td>
                    </tr>
                    <tr>
                        <td>Xanadu</td>
                        <td>ELO</td>
                        <td><a href="http://open.spotify.com/track/3BD6WXtv6V6k978ZVJ67yt"><i class="fab fa-spotify"></i></a></td>
                    </tr>
                    <tr>
                        <td>You better you bet</td>
                        <td>The Who</td>
                        <td><a href="https://open.spotify.com/track/6krAkpKf8NW11bOfNkxpmx"><i class="fab fa-spotify"></i></a></td>
                    </tr>
                </tbody>
            </table>
        </p>
    </div>
</section>