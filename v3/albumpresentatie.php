<section id="showcase">
    <div class="container">
        <h1><?php print($c['title']);?></h1>
    </div>
</section>

<section id="main">
    <div class="container">
        <p>
            <img class="bandphoto" alt="Albumpresentatie" src="img/eigentijdsehelden600.png"/>
            <?php print($c['p1']);?>
        </p>
        <p>
            <?php print($c['p2']);?>
        </p>
        <p>            
            <?php print($c['p3']);?>
        </p>
        <p>            
            <?php print($c['p4']);?>
        </p>
    </div>
</section>