<section id="showcase">
    <div class="container">
        <h1><?php print $c['touch'];?></h1>
    </div>
</section>

<section id="main">
    <div class="container">
        <h3><i class="mainicon fas fa-at fa-2x"></i><?php print $c['contact'];?></h3>
        <p>
            <?php print $c['call'];?>
        </p>
        <p>
            <?php print $c['numbers'];?>
        </p>
        <p>
            <?php print $c['email'];?>
        </p>
    </div>
</section>
