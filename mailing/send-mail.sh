#!/bin/bash

echo Royal Teazer Mail Sender

if [ "$#" -ne 1 ]; then
    echo "Illegal number of parameters"
else

    echo "Reading: " $1

    EMAIL=`cat $1 | grep -oE "To: [^<]+<([^>]+)>" | grep -oE "<.*>" | grep -oE "[^<^>]*"`
    echo "Mailing to: " $EMAIL

    curl -k --ssl-reqd smtp://127.0.0.1:1025 \
    --mail-from info@royalteazer.nl \
    --mail-rcpt $EMAIL \
    --mail-rcpt info@royalteazer.nl \
    --user 'paul@twisq.nl:RTjyQuBCvqhP2icYpaXzUA' \
    --upload-file $1
fi